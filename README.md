# Todo-List
Application de Todo-List avec plusieurs fonctions de tri

## Cloner le projet
```
git@gitlab.com:CopainYanis/todo-list-vue.git
https://gitlab.com/CopainYanis/todo-list-vue.git
```
## Installation du projet
```
npm install
```
### Compiler et hot-reloads pour le developement
```
npm run serve
```
